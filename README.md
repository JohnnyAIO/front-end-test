## REIGN TEST FRONTEND DEVELOPER

![Logo](https://images.ctfassets.net/fo9twyrwpveg/2l4UlCCVuAQaw2WkKig66I/970c643d4dbba535ded9415b7df30241/Logo-Reign-2021-negro.svg)

## Welcome

This project was develop by the following technologies, using the rest api that mention in a document.

[Angular +12](https://github.com/nestjs/nest)
[Nest](https://github.com/nestjs/nest)
[MongoDB](https://github.com/nestjs/nest)

## Installation

You have 2 folders: back-end and front-end, you need two terminals to install once por once.

```bash
$ npm install
```

## Running the app for Backend Server

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod

PORT: 8080

For the first time to load all articles you need to do this following steps:

You need go to open browser 
and then go to: http://localhost:8080/articles/create to generate articles
```

## Running the app for Frontend Server

```bash
# development
$ ng serve

PORT: 80

Go to the main page, and will shop up all the articles, 
if you would like to visit this articles, you can click.

```

## Docker Compose

```bash
# development
$ docker-compose build
$ docker-compose up

PORT: 80

```

## END POINTS

GET: http://localhost:8080/articles/create (Create all articles first important)

GET: http://localhost:8080/articles/find (Get all the articles order by Date)

DELETE: http://localhost:8080/articles/delete/${ObjectId} (Delete the article that you selected)


## License

Nest and Angular is [MIT licensed](LICENSE).
