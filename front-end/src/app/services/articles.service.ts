import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ArticlesService {

  constructor(private http: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': '*',
      'Access-Control-Allow-Methods': 'GET,POST,OPTIONS,DELETE,PUT',
    })
  }

  getArticles() {
    return this.http.get('http://localhost:8080/articles/find', this.httpOptions);
  }

  deleteArticle(id: string) {
    return this.http.delete('http://localhost:8080/articles/delete/' + id, this.httpOptions);
  }
}
