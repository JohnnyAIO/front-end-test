export interface Articles {

    created_at: Date;

    order_date: Date;

    title: string;

    url: string;

    author: string;

    points: string;

    story_text: string;

    comment_text: string;

    num_comments: string;

    story_id: number;

    story_title: string;

    story_url: string;

    created_at_i: Date; //UNIX FORMAT

    _tags: string[];

    objectID: string;

    _highlightResult: {
        autor: {
            value: string,
            matchLevel: string,
            matchedWords: string[]
        },
        comment_text: {
            value: string,
            matchLevel: string,
            fullyHighlighted: boolean,
            matchedWords: string[],
        },
        story_title: {
            value: string,
            matchLevel: string,
            matchedWords: string[],
        },
        story_url: {
            value: string,
            matchLevel: string,
            matchedWords: string[],
        },
    };
}
