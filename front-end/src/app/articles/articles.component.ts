import { Component, OnInit } from '@angular/core';
import { isToday, isYesterday, format } from 'date-fns';
import { ArticlesService } from '../services/articles.service';
import { Articles } from '../interfaces/articles';

@Component({
  selector: 'app-articles',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.scss']
})
export class ArticlesComponent implements OnInit {
  public articles: Articles[] = [];
  public articles_backup: Articles[] = [];
  constructor(protected article: ArticlesService) { }

  ngOnInit(): void {
    this.getAllArticles();
  }

  getAllArticles() {
    this.article.getArticles().subscribe((response: any) => {
      this.articles_backup = response.msg;
      this.articles_backup.forEach((element: any) => {
        element.order_date = this.formatDate(element.created_at);
      });
      this.articles = this.articles_backup;
    });
  }

  formatDate(date_at: string) {
    let date = new Date(date_at);
    if (isToday(date)) {
      return format(date, 'h:mm a');
    } else if (isYesterday(date)) {
      return 'Yesterday';
    } else {
      return format(date, 'MMM D');
    }
  }

  deleteArticle(article_id: any) {
    this.article.deleteArticle(article_id).subscribe((response: any) => {
      if (response) {
        console.log('Successfully Elimited');
        this.getAllArticles();
      }
    });
  }

}
