import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type ArticleDocument = Article & Document;

@Schema()
export class Article extends Document {

    @Prop()
    created_at: Date;

    @Prop()
    title: string;

    @Prop()
    url: string;

    @Prop()
    author: string;

    @Prop()
    points: string;

    @Prop()
    story_text: string;

    @Prop()
    comment_text: string;

    @Prop()
    num_comments: string;

    @Prop()
    story_id: number;

    @Prop()
    story_title: string;

    @Prop()
    story_url: string;

    @Prop()
    created_at_i: number; //UNIX FORMAT

    @Prop()
    _tags: string[];

    @Prop()
    objectID: string;

    @Prop({ type: Object })
    _highlightResult: {
        autor: {
            value: string,
            matchLevel: string,
            matchedWords: string[]
        },
        comment_text: {
            value: string,
            matchLevel: string,
            fullyHighlighted: boolean,
            matchedWords: string[],
        },
        story_title: {
            value: string,
            matchLevel: string,
            matchedWords: string[],
        },
        story_url: {
            value: string,
            matchLevel: string,
            matchedWords: string[],
        },
    };
}

export const ArticleSchema = SchemaFactory.createForClass(Article);