import { IsNumber, IsString, IsArray, IsObject, IsDate } from "class-validator";

export class CreateArticleDto {

    @IsString()
    title: string;

    @IsString()
    url: string;

    @IsString()
    author: string;

    @IsString()
    points: string;

    @IsString()
    story_text: string;

    @IsString()
    comment_text: string;

    @IsString()
    num_comments: string;

    @IsNumber()
    story_id: number;

    @IsString()
    story_title: string;

    @IsString()
    story_url: string;

    @IsDate()
    created_at_i: Date; //UNIX FORMAT

    @IsArray()
    _tags: string[];

    @IsString()
    objectID: string;

    @IsObject()
    _highlightResult: {
        autor: {
            value: string,
            matchLevel: string,
            matchedWords: string[]
        },
        comment_text: {
            value: string,
            matchLevel: string,
            fullyHighlighted: boolean,
            matchedWords: string[],
        },
        story_title: {
            value: string,
            matchLevel: string,
            matchedWords: string[],
        },
        story_url: {
            value: string,
            matchLevel: string,
            matchedWords: string[],
        },
    };

}
