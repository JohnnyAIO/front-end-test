import { Controller, Get, Post, Body, Patch, Param, Delete, Res, HttpStatus } from '@nestjs/common';
import { ArticlesService } from './articles.service';

@Controller('articles')
export class ArticlesController {
  constructor(private readonly articlesService: ArticlesService) { }

  @Get('create')
  create(@Res() res) {
    this.articlesService.create();
    res.status(200).send({ msg: 'Import Info Complete :D' });
  }

  @Get('find')
  findAll(@Res() res) {
    this.articlesService.findAll().then((response) => {
      if (!response) {
        res.status(400).send({ msg: 'There is an error of the response D:' });
      }
      if (response) {
        res.status(200).send({ msg: response });
      }

    });
  }

  @Delete('delete/:id')
  remove(@Param('id') id: any, @Res() res) {
    this.articlesService.remove(+id).then((response) => {
      if (!response) {
        res.status(400).send({ msg: 'There is an error of the response D:' });
      }
      if (response) {
        res.status(200).send({ msg: 'Delete Succesfully :D' });
      }
    });
  }
}
