import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Article, ArticleDocument } from '../schemas/article.schema';
import axios from 'axios';

@Injectable()
export class ArticlesService {

  @InjectModel(Article.name) private articleModel: Model<ArticleDocument>

  create() {
    let message = '';
    axios.get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs').then((articles) => {
      let data = articles.data.hits;
      // console.log(data);
      if (data) {
        data.forEach((element) => {
          this.articleModel.findOneAndUpdate({ objectID: element.objectID }, element, { upsert: true }, (err, response) => {
            if (response) message = "importing complete";
            if (err) message = "There is an error";
          });
        });
      }
    }).catch((error) => {
      console.log(error);
      message = error;
    });
    return message;
  }

  findAll() {
    return this.articleModel.find().sort({ created_at: -1 }).exec();
  }

  remove(id: any) {
    return this.articleModel.findOneAndDelete({ objectID: id }).exec();
  }
}
